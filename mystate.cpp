#include<stdio.h>
#include<time.h>
#include<sys/stat.h>
#include<unistd.h>
#include<sys/types.h>
#include<errno.h>
#include<stdlib.h>
 
int main(int argc, char **argv)
{
    struct stat buf;
    /*检查参数个数*/
    if(argc!=2)
    {
        perror("Usage:my_stat <filename>\n");
        exit(1);
    }
 
    /*获取文件属性*/
    if( stat(argv[1], &buf) ==-1 )
    {
        perror("stat:");
        exit(1);
    }
 
    /*打印文件属性*/
    printf("device is: %d\n",buf.st_dev);//文件设备编号
    printf("inode is : %d\n",buf.st_ino);//文件ｉ节点标号
    printf("mode is: %o\n",buf.st_mode);//文件类型与存取权限
    printf("number of hard links is: %d\n",buf.st_nlink);//硬链接数目
    printf("user ID of owner is: %d\n",buf.st_uid);//用户所有者的用户ｉｄ
    printf("group ID of owner is: %d\n",buf.st_gid);//用户所有者组ｉｄ
    printf("device type (if inode device )is: %d\n",buf.st_rdev);//若为设备文件则为设备编号
 
    printf("total size ,in bytes is: %d\n",buf.st_size);//文件大小，以字节计算
    printf("blocksize for filesystem I/O is: %d\n",buf.st_blksize);//文件系统ＩＯ缓冲区大小
    printf("number of blocks allocated is: %d\n",buf.st_blocks);//占有文件区块个数，一般一个区块大小通常５１２字节
    
    printf("time of lasst access is: %s",ctime(&buf.st_atime));//文件最近一次被访问时间
    printf("time of last modification is: %s",ctime(&buf.st_mtime));//最后一次被修改的时间
    printf("time of last chage is: %s",ctime(&buf.st_ctime));//最近一次被更改时间
 
    return 0;
}
